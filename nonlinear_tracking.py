import numpy as np
import time

# Normalized coordinates
# x: Normalized particle coordinate x in units of [m^(1/2)]
# px: Normalized particle divergence x' in units of [m^(1/2)]


def make_probe_particles_string(
    nparticles = 30,  # particles to simulate
    xmax = 17,        # maximum initial x of particles
    pxmax = 0,        # maximum initial px of particles
):
    """Create particles on a string from phase space origin to given maximum coordinates
    """
    return np.linspace((0,0), (xmax,pxmax), nparticles).T
    
def make_probe_particles_grid(
    nparticles = 30,  # number of particles for either dimension
    xmax = 17,        # maximum initial x of particles
    pxmax = 0,        # maximum initial px of particles
):
    """Create particles on a grid
    """
    x = np.linspace(-xmax,xmax,nparticles)
    px = np.linspace(-pxmax,pxmax,nparticles)
    x, px = np.meshgrid(x, px)
    return np.array((x.flatten(),px.flatten()))
    
def make_probe_particle_beam_uniform(
    nparticles = 10000,  # number of particles
    epsilon = 1**2,      # beam emittance, i.e. x²+px² (since twiss beta=1, alpha=0)
):
    """Create particles randomly uniform distributed in a circle of given size
    """
    theta = np.random.uniform(0, 2*np.pi, nparticles)
    radius = np.random.uniform(0, epsilon, nparticles) ** 0.5
    x = radius * np.cos(theta)
    px = radius * np.sin(theta)
    return np.array((x, px))

def make_probe_particle_beam_gaussian(
    nparticles = 10000,  # number of particles
    epsilon = 1**2,      # beam emittance, i.e. x²+px² (since twiss beta=1, alpha=0)
    sigmas = 1,          # number of standard deviations the given emittance value corresponds to
):
    """Create particles randomly gaussian distributed
    """
    return np.random.multivariate_normal([0,0], ((epsilon**.5)/sigmas)**2 * np.identity(2), nparticles).T


    

def simulation(
    ncells = 1,      # number of FODO cells in machine
    q = 0.203,       # tune of machine
    k0l = 0,         # kicker strength (can also be a function with signature lambda(turn) -> k0l)
    k2l = 0.1,       # sextupole strength [1/m²]
    k3l = 0.0,       # octupole strength [1/m³]
    nturns = 10000,  # number of turns in machine
    aperture = None, # consider particles with |x| > aperture lost, i.e. mark as np.nan [m]
    twiss_beta = 1,
    particles = np.zeros((2, 0)), # initial normalized particle coordinates (x, x') [m, 1]
    progress_callback = None,
):
    """
    2D particle tracking simulation of a simple circular accelerator built of identical cells.
    Each cell is linear with given tune and contains nonlinear multipoles (sextupole and an octupole) in thin lense approximation at its end.
    For simplicity, twiss functions of beta=1 and alpha=0 are assumed at the location of the multipoles, such that the transfer map of the cell is a simple rotation in phase space.
    
    All values correspond to the location of the kicker/sextupole/octupole...
    
    :param q: tune (phase advance of all cells divided by 2pi) in the linear case
    :param k2l: Effective strength of thin sextupole k2l = k_2*l in units of [1/m²]
                Sign convention follows MAD-X, such that the normalized sextupole strength is S=-1/2*beta_x^(3/2)*k2l in units of [m^(-1/2)]
                
    :param twiss_beta: Betafunction in units of [m]
    :param particles: Normalized particle coordinate (x, x') in units of [m^(1/2), m^(1/2)]
    
    """
    
    mu = 2*np.pi*q/ncells   # phase advance per cell
    S = - 1/2 * twiss_beta**1.5 * k2l # normalized sextupole strength
    # TODO: normalized kick and octupole
    if twiss_beta != 1 and (k0l or k3l):
        raise NotImplemented("Definitions for kicker ans octupole are not corrected for twiss_beta!=1 yet")
    

    # linear map of FODO cell in normalized coordinates
    M = np.array([(np.cos(mu),np.sin(mu)), (-np.sin(mu),np.cos(mu))])
    
    if not callable(k0l):
        k0l_const = k0l
        k0l = lambda _: k0l_const

    def cell(p, t):
        x, px = M.dot(p) # linear dynamics
        px +=        k0l(t)     # thin kicker
        px += S * x**2 # thin sextupole kick
        px += -1/6 * k3l * x**3 # thin octupole kick
        if aperture is not None: x = np.where(np.abs(x) > aperture, np.nan, x)
        return np.array((x, px))

    # tracking
    particle_coordinates = np.empty((2, particles.shape[1], nturns+1))
    particle_coordinates[:,:,0] = particles
    callback = time.perf_counter()
    for t in range(nturns):
        for c in range(ncells):
            particles = cell(particles, t)
        particle_coordinates[:,:,t+1] = particles

        if progress_callback is not None and time.perf_counter() > callback + 0.1:
            callback = time.perf_counter()
            progress_callback(t/nturns)

    x, px = particle_coordinates
    particle_amplitude = (x**2 + px**2)**0.5
    particle_angle = -np.arctan2(px, x)
    particle_tune = np.diff(particle_angle, axis=1)/(2*np.pi)%1
    particle_tune[particle_amplitude[:,:-1] == 0] = q  # tune of synchronous particle equals design tune, but can not be calculated with arctan2 method

    if progress_callback is not None: progress_callback(1)
    return ncells, q, k2l, k3l, particle_coordinates, particle_amplitude, particle_angle, particle_tune,



def kobayashi_hamiltonian(x, px, d, S):
    """Kobayashi hamiltonian for a system with a sextupole
    :param x: Normalized particle coordinate x in units of [m^(1/2)]
    :param px: Normalized particle divergence x' in units of [m^(1/2)]
    :param d: Distance d=q-r of (linear) tune q to the nearby 3rd integer resonance r in units of [1]
    :param S: Normalized sextupole strength S in units of [m^(-1/2)]
    :returns: Value of the hamiltonian in units of [m]
    """
    return 3*np.pi*d*(x**2 + px**2) + S/4*(3*x*px**2 - x**3)


def kobayashi_hamiltonian_normalized(x, px, h):
    """Kobayashi hamiltonian for a system with a sextupole, normalized to the value of the hamiltonian at the separatrix
    :param x: Normalized particle coordinate x in units of [m^(1/2)]
    :param px: Normalized particle divergence x' in units of [m^(1/2)]
    :param h: Normalized radius of inscribed circle of separatrix h=4*pi*d/S in units of [m^(1/2)]
    :returns: Normalized value of the hamiltonian in units of [1]
              A value of 0 corresponds to the origin of phase space and a value of 1 corresponds to the separatrix
    """
    return kobayashi_hamiltonian(x, px, 1/(4*np.pi*h**2), 1/h**3)
    
def triangular_amplitude(x, px, h):
    """Amplitude relative to separatrix according to the Kobayashi Hamiltonian
    :param h: size of the separatrix triangle (radius of inscribed circle)
    :returns: relative amplitude scaled to triangle (0=center, 1=inscribed circle), i.e. sqrt(H/H_separatrix)
    see kobayashi_hamiltonian_normalized
    """
    # i.e. compare to Kobayashi Hamiltonian at separatrix H / ((4*pi*d)**3/S**2)
    return np.sqrt(kobayashi_hamiltonian_normalized(x, px, h))

def to_half_intervall(q):
    """Returns the corresponding fractional tune value in the half interval [0;0.5)
    """
    q = q-np.floor(q)
    q = np.where(q < 0, q+1, q)
    return np.min([q, 1-q], axis=0)
