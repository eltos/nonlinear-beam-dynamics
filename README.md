# Nonlinear beam dynamics

An interactive jupyter notebook demonstrating nonlinear beam dynamics by tracking particles in a simplified lattice with sextupole and octupole magnets.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eltos%2Fnonlinear-beam-dynamics/main?labpath=nonlinear_tracking_interactive.ipynb)

![Screenshot](nonlinear_tracking_example.png)
