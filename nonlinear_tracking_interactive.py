import ipywidgets as widgets
import matplotlib.pyplot as plt
import numpy as np
from nonlinear_tracking import *


class FigurePhaseSpace(widgets.Output):

    def __init__(self):
        super().__init__()

        with self:
            self.fig, self.ax = plt.subplots(dpi=100, constrained_layout=True, num='Figure 1: Nonlinear beam dynamics for a simplified model')
            self.ax.set_xlabel('$x$')
            self.ax.set_ylabel('$p_x$')
            self.ax.set_title('Phase space')

            self.text = plt.text(0.02,0.98, '...', transform=plt.gca().transAxes, va='top')
            cmap = plt.get_cmap('jet_r').copy()
            cmap.set_bad(color='k')
            self.colormap = plt.cm.ScalarMappable(cmap=cmap)
            self.colorbar = plt.colorbar(self.colormap)

    def update(self, ncells, Q, k2L, k3L, particle_coordinates, particle_amplitude, particle_angle, particle_tune, *, average=False):
        # update plot
        self.text.set_text(f'$Q={Q:g}$\nCells: {ncells}\n$k_2L={k2L:g}$\n$k_3L={k3L:g}$')
        self.colorbar.set_label('Average phase advance per turn' if average else 'Phase advance per turn')


        color = particle_tune.mean(axis=1).repeat(particle_tune.shape[-1]).reshape(particle_tune.shape) if average else particle_tune
        qmin, qmax = np.nanmin(color.min(axis=1)), np.nanmax(color.max(axis=1))
        self.colormap.norm = plt.Normalize(vmin=qmin, vmax=qmax)
        self.colorbar.update_normal(self.colormap)

        while self.ax.lines: self.ax.lines.pop()
        while self.ax.collections: self.ax.collections.pop()

        self.ax.scatter(particle_coordinates[0,:,:-1].flatten(), particle_coordinates[1,:,:-1].flatten(), (72/self.fig.dpi)**2, self.colormap.to_rgba(color.flatten()), ',', lw=0)

        self.ax.axis('equal')
        zoom = 1.2*np.max(particle_amplitude[:,0]) # maximum initial coordinate in x or px
        self.ax.set_xlim(-zoom, zoom)
        self.ax.set_ylim(-zoom, zoom)


class FigureDetuning(widgets.Output):

    def __init__(self):
        super().__init__()

        with self:
            self.fig, self.ax = plt.subplots(dpi=100, constrained_layout=True, num='Figure 2: Amplitude detuning for a simplified model.')
            self.ax.set_title('Amplitude detuning')
            self.ax.set_ylabel('Amplitude $\\sqrt{x^2+p_x^2}$')

            self.text = self.ax.text(0,0.02, '...', transform=self.ax.transAxes, va='bottom')
            cmap = plt.get_cmap('jet_r').copy()
            cmap.set_bad(color='k')
            self.colormap = plt.cm.ScalarMappable(cmap=cmap)


    def update(self, ncells, Q, k2L, k3L, particle_coordinates, particle_amplitude, particle_angle, particle_tune, *, average=False):

        # update plot
        self.text.set_text(f'$Q={Q:g}$\nCells: {ncells}\n$k_2L={k2L:g}$\n$k_3L={k3L:g}$')
        self.ax.set_xlabel('Average phase advance per turn' if average else 'Phase advance per turn')

        color = particle_tune.mean(axis=1).repeat(particle_tune.shape[-1]).reshape(particle_tune.shape) if average else particle_tune
        qmin, qmax = np.nanmin(color.min(axis=1)), np.nanmax(color.max(axis=1))
        self.colormap.norm = plt.Normalize(vmin=qmin, vmax=qmax)


        while self.ax.lines: self.ax.lines.pop()
        while self.ax.collections: self.ax.collections.pop()


        # bare tune
        self.ax.axvline(Q, c='k', ls='--', zorder=-1, label=f'$Q={Q:g}$')


        if average:
            amax = 1.2*np.nanmax(particle_amplitude.mean(axis=1))

            # average amplitude detuning
            self.ax.scatter(particle_tune.mean(axis=1), particle_amplitude.mean(axis=1), c=self.colormap.to_rgba(particle_tune.mean(axis=1)))

            # resonances
            n_max = 10
            for n in range(1,n_max+1):
                for i in range(1, n):
                    if i > 1 and n%i == 0: continue # lower order, since fraction can be reduced
                    if i/n < 1.1*qmin-0.1*qmax or i/n > 1.1*qmax-0.1*qmin: continue # not relevant since outside data range ±10%
                    self.ax.axvline(i/n, c='lightgrey',ls=(0, [7,2]+[1,2]*n),zorder=-2)

        else:
            amax = 1.2*np.max(particle_amplitude[:,0])

            # turn-by-turn amplitude detuning
            self.ax.scatter(particle_tune.flatten(), particle_amplitude[:,:-1].flatten(), (72/self.fig.dpi)**2, self.colormap.to_rgba(particle_tune.flatten()), ',', lw=0)

            # each multipole can change the tune at a given amplitude A not more than (beta=1 in our model):
            # dQ = 1/(4*pi) * k2L * A           # for sextupoles
            # dQ = 1/(4*pi) * 1/3 * k3L * A^2   # for octupoles
            A = np.linspace(0, amax, 100)
            dQ = 1/(4*np.pi) * ( np.abs(k2L) * A + 1/3 * np.abs(k3L) * A**2 ) * ncells
            self.ax.plot(Q + dQ, A, c='k', ls=':', zorder=-1)
            self.ax.plot(Q - dQ, A, c='k', ls=':', zorder=-1)


        self.ax.set_xlim(1.2*qmin - 0.2*qmax, 1.2*qmax - 0.2*qmin)
        self.ax.set_ylim(0, amax)
        self.text.set(x=0.04 if Q > (qmin+qmax)/2 else 0.96, ha='left' if Q > (qmin+qmax)/2 else 'right')


class Controls(widgets.HBox):

    def __init__(self, update_callback):
        super().__init__()

        self.updates = True
        self.update_callback = update_callback

        self.param_q = widgets.BoundedFloatText(value=0.203, min=0, max=1, step=0.001, description='Betatron tune q')
        self.param_k2l = widgets.FloatText(value=0.1, step=0.01, description='Sextupole strength k2L')
        self.param_k3l = widgets.FloatText(value=0.0, step=0.005, description='Octupole strength k3L')
        self.param_ncells = widgets.BoundedIntText(value=1, min=1, max=1e99, description='Number of cells')
        self.param_nparticles = widgets.BoundedIntText(value=50, min=1, max=1e99, description='Number of particles')
        self.param_nturns = widgets.BoundedIntText(value=10000, min=1, max=1e99, step=1000, description='Number of turns')
        self.param_xmax = widgets.FloatText(value=17, step=1, description='Maximum initial x')
        self.param_pxmax = widgets.FloatText(value=-2, step=1, description='Maximum initial px')
        self.presets = widgets.Dropdown(description='Preset', options=[
            ('Select preset', {}),
            ('Sextupole 5th order resonance', dict(ncells=1, q=0.203, k2l=0.1, k3l=0, nparticles=50, xmax=17, pxmax=-2)),
            ('Sextupole 4th order resonance', dict(ncells=1, q=0.2525, k2l=0.1, k3l=0, nparticles=50, xmax=17, pxmax=-5)),
            ('Sextupole 3rd order resonance', dict(ncells=1, q=0.3333, k2l=0.1, k3l=0, nparticles=50, xmax=14, pxmax=0)),
            ('Sextupole close to 3rd order resonance', dict(ncells=1, q=0.331, k2l=0.1, k3l=0, nparticles=50, xmax=2, pxmax=0)),
            ('Sextupole 2nd and 7th order resonance, 3 cells', dict(ncells=3, q=0.51, k2l=0.1, k3l=0, nparticles=70, xmax=14, pxmax=-4)),
            ('Octupole 4rd order resonance', dict(ncells=1, q=0.244, k2l=0, k3l=0.01, nparticles=50, xmax=17, pxmax=17)),
            ('Beam split in 4 (tune ramp from 0.25 to 0.255)', dict(ncells=1, q=0.255, k2l=0.1, k3l=0.0, nparticles=50, xmax=18, pxmax=-6)),
            ('Beam split in 5 (tune ramp from 0.20 to 0.21)', dict(ncells=1, q=0.21, k2l=0.2, k3l=0.015, nparticles=50, xmax=11, pxmax=-6)),

        ])
        self.presets.observe(lambda p: self.preset(**p.new) if p.new else None, 'value')
        self.average = widgets.Checkbox(value=True, description='Show averaged phase advance')
        self.average.observe(lambda _: self.update_callback(only_visual=True), 'value')
        self.progress = widgets.FloatProgress(min=0,max=1,description='Progress')

        controls = widgets.HBox([
            widgets.VBox([self.param_q, self.param_k2l, self.param_k3l, self.param_ncells]),
            widgets.VBox([self.param_nparticles, self.param_xmax, self.param_pxmax, self.param_nturns]),
        ])

        def walk(box):
            for w in box.children:
                if isinstance(w, widgets.Box):
                    walk(w)
                else:
                    w.style = {'description_width': '70%' or 'initial'}
                    w.observe(self.change, 'value')
        walk(controls)

        self.children = [controls, widgets.VBox([self.presets, self.progress, self.average])]

    def change(self, _=None):
        if not self.updates: return
        self.presets.index = 0
        self.progress.value = 0
        self.update_callback(progress_callback=self.progress_callback)
        self.progress.value = 1#

    def progress_callback(self, p):
        self.progress.value = p

    def preset(self, **preset):
        self.updates = False
        for key, value in preset.items():
            if hasattr(self, 'param_%s'%key):
                getattr(self, 'param_%s'%key).value = value
            elif key == 'average':
                self.average.value = value
        self.updates = True
        self.change()

    def parameters(self, which=('ncells','q','k2l','k3l','nparticles','nturns','xmax','pxmax')):
        return {key: getattr(self, 'param_%s'%key).value for key in which if hasattr(self, 'param_%s'%key)}

    def appearance(self):
        return dict(average=self.average.value)


class Dashboard(widgets.VBox):

    def __init__(self, **presets):
        super().__init__()

        plt.ioff()

        # elements
        self.figure = FigurePhaseSpace()
        self.figure2 = FigureDetuning()
        self.controls = Controls(self.update)
        self.children = [self.controls,
                         widgets.Label(),
                         widgets.HBox([self.figure.fig.canvas, self.figure2.fig.canvas]),
                         widgets.HTML('<p style="line-height: unset;">The black dashed line marks the bare tune, the gray dash-dotted lines mark resonances (order of resonance is indicated by number of dots), and the black dotted lines indicate the maximum possible tune change for the selected sextupole/octupole strength.</p>')]

        plt.ion()

        # initial update
        self.data = None
        self.controls.preset(**presets)
        self.update()


    def update(self, progress_callback=lambda x: None, only_visual=False):
        progress_callback(0)

        if not self.data or not only_visual:
            # simulation
            with np.errstate(all='ignore'): # do not print overflow errors (lost particles)
                
                particles = make_probe_particles_string(**self.controls.parameters(['nparticles','xmax','pxmax']))
                self.data = simulation(**self.controls.parameters(['ncells','q','k2l','k3l','nturns']), particles=particles, progress_callback=lambda p: progress_callback(0.9*p))

        # update plot
        self.figure.update(*self.data, **self.controls.appearance())
        self.figure2.update(*self.data, **self.controls.appearance())
        progress_callback(1)
